package com.nTier.tmobile;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TmobileOnePlanTest {


	@Test
	public void test() {
		TmobileOnePlan n=new TmobileOnePlan(1,0);
		Person p=new Person("Bob","Martin",2342345678l,false,1);
		assertEquals(40,n.calcCost(p),0);
	}
	
	@Test
	public void testDiscount() {
		Person person = new Person("Bob","Martin",2342345678l,true,1);
		TmobileOnePlan tMobileOnePlan = new TmobileOnePlan();
		assertEquals(35, tMobileOnePlan.calcDiscount(40), .001);
	}

}
