package com.nTier.tmobile;

class TmobileOnePlan {
	int numberOfLines;
	int numberOfPhonesOnContract;
	final static double costPerLine = 40;

	public TmobileOnePlan(int numberOfLines, int numberOfPhonesOnContract) {
		this.numberOfLines = numberOfLines;
		this.numberOfPhonesOnContract = numberOfPhonesOnContract;
	}

	double calcDiscount(double cost) {
		return cost - 5;

	}

	public double calcCost(Person p) {
		if(p.isBillPayType())
		{
			return ((this.numberOfLines * costPerLine)-this.calcDiscount((this.numberOfLines * costPerLine)));
		}
		return (this.numberOfLines * costPerLine);
	}

	public TmobileOnePlan() {
	}

}
