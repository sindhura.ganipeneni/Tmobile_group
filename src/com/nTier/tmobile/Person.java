package com.nTier.tmobile;


public class Person {
	
	String firstName;
	String lastName;
	long phoneNumber;
	boolean billPayType;
	int phoneIEMI;
	String planStatus;
	int noOFLines;
	public Person(String firstName, String lastName, long phoneNumber, boolean billPayType,int noOfLines) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.billPayType = billPayType;
		this.noOFLines=noOfLines;
	}
	public int getNoOFLines() {
		return noOFLines;
	}
	public void setNoOFLines(int noOFLines) {
		this.noOFLines = noOFLines;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public boolean isBillPayType() {
		return billPayType;
	}
	public void setBillPayType(boolean billPayType) {
		this.billPayType = billPayType;
	}
	public int getPhoneIEMI() {
		return phoneIEMI;
	}
	public void setPhoneIEMI(int phoneIEMI) {
		this.phoneIEMI = phoneIEMI;
	}
	public String getPlanStatus() {
		return planStatus;
	}
	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}
	

}

